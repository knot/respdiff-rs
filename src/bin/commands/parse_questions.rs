use anyhow::Result;
use clap::Args;
use domain::base::Message;
use lmdb::Transaction;
use log::{debug, info};
use pcap::{Capture, Linktype, PacketCodec};
use pnet::packet::ip::IpNextHeaderProtocols;
use pnet::packet::ipv4::Ipv4Packet;
use pnet::packet::ipv6::Ipv6Packet;
use pnet::packet::udp::UdpPacket;
use pnet::packet::Packet;
use respdiff::database::{self, nssetsdb, questionsdb};
use respdiff::error::{Error, PcapError};
use respdiff::{NameServerSet, Question};

use std::path::PathBuf;

use crate::commands::{Executable, Respdiff};

const DLT_RAW: Linktype = Linktype(12);
static CHUNK_SIZE: usize = 100000;

#[derive(Debug, Args)]
pub struct ParseQuestions {
    /// Input PCAP to read DNS questions from.
    pcap: PathBuf,
}

#[derive(Debug)]
enum QuestionCodecLinkType {
    RawIp,
    Ethernet,
}

struct QuestionCodec {
    linktype: QuestionCodecLinkType,
}
impl PacketCodec for QuestionCodec {
    type Item = Result<Question, Error>;

    fn decode(&mut self, packet: pcap::Packet) -> Self::Item {
        let data = match self.linktype {
            QuestionCodecLinkType::Ethernet => {
                if packet.data.len() <= 14 {
                    return Err(PcapError::ShortPacket.into());
                }
                match (packet.data[12], packet.data[13]) {
                    (0x86, 0xdd) | (0x08, 0x00) => &packet.data[14..packet.data.len()],
                    _ => return Err(PcapError::UnsupportedNetwork.into()),
                }
            }
            QuestionCodecLinkType::RawIp => {
                if packet.data.is_empty() {
                    return Err(PcapError::ShortPacket.into());
                }
                packet.data
            }
        };
        let payload: Vec<u8> = match data[0] >> 4 {
            4 => {
                let ip4 = Ipv4Packet::new(data).ok_or(PcapError::ShortPacket)?;
                if ip4.get_next_level_protocol() != IpNextHeaderProtocols::Udp {
                    return Err(PcapError::UnsupportedTransport.into());
                }
                let udp = UdpPacket::new(ip4.payload()).ok_or(PcapError::ShortPacket)?;
                udp.payload().into()
            }
            6 => {
                let ip6 = Ipv6Packet::new(data).ok_or(PcapError::ShortPacket)?;
                if ip6.get_next_header() != IpNextHeaderProtocols::Udp {
                    return Err(PcapError::UnsupportedTransport.into());
                }
                let udp = UdpPacket::new(ip6.payload()).ok_or(PcapError::ShortPacket)?;
                udp.payload().into()
            }
            _ => return Err(PcapError::UnsupportedNetwork.into()),
        };
        let message: Message<Vec<u8>> = Message::from_octets(payload)?;
        let question: Question = message.try_into()?;
        Ok(question)
    }
}

impl Executable for ParseQuestions {
    fn exec(&self, args: &Respdiff) -> Result<()> {
        let env = args.env()?;
        let qdb = database::open_db(&env, questionsdb::NAME, true)?;
        let nsdb = database::open_db(&env, nssetsdb::NAME, true)?;

        let cap = Capture::from_file(&self.pcap)?;
        let linktype = cap.get_datalink();
        let codec = match linktype {
            DLT_RAW => Ok(QuestionCodec {
                linktype: QuestionCodecLinkType::RawIp,
            }),
            Linktype::ETHERNET => Ok(QuestionCodec {
                linktype: QuestionCodecLinkType::Ethernet,
            }),
            _ => Err(PcapError::UnsupportedLinktype(
                linktype.get_name()?,
                linktype.0,
            )),
        }?;

        let questions = cap.iter(codec).filter_map(|item| match item {
            Ok(qres) => match qres {
                Ok(question) => Some(question),
                Err(e) => {
                    debug!("failed to parse as DNS message: {e}");
                    None
                }
            },
            Err(e) => {
                debug!("failed to decode packet: {e}");
                None
            }
        });

        let mut written = 0;
        let mut incremented = 0;

        let mut txn = env.begin_rw_txn()?;
        let mut i = 0;
        for question in questions {
            let qbuf: Vec<u8> = question.qbuf();
            let count = questionsdb::increment(qdb, &mut txn, &qbuf)?;
            if count == 1 {
                if let Err(Error::NotFoundInDb(_, db)) =
                    nssetsdb::get_nsset(nsdb, &txn, &question.qname)
                {
                    let keystr = question.qname.to_string();
                    debug!("adding \"{keystr}\" to LMDB \"{db}\"");
                    let entry = nssetsdb::NsSetEntry {
                        domain: question.qname,
                        nsset: NameServerSet::new(),
                    };
                    nssetsdb::write(nsdb, &mut txn, &entry)?;
                };
                written += 1;
            } else {
                incremented += 1;
            }
            i += 1;
            if i == CHUNK_SIZE {
                i = 0;
                let processed = written + incremented;
                info!("processed: {processed}");
            }
        }
        info!("committing transaction to LMDB");
        txn.commit()?;

        info!("total questions written: {written}");
        info!("total questions incremented: {incremented}");

        Ok(())
    }
}

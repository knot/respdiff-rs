use crate::commands::{Executable, Respdiff};
use anyhow::Result;
use clap::Args;
use domain::base::Dname;
use lmdb::Transaction;
use log::info;
use respdiff::{
    self,
    database::{self, nssetsdb},
    Resolver,
};
use std::net::IpAddr;

#[derive(Debug, Args)]
pub struct SnapshotNsset {
    /// IP address of resolver to send queries to.
    #[arg(short, long)]
    resolver: Option<IpAddr>,
    /// Port of the resolver.
    #[arg(short, long, default_value_t = 53)]
    port: u16,
    /// Domain name for which to obtain the NS set.
    domain: Dname<Vec<u8>>,
}

impl Executable for SnapshotNsset {
    #[tokio::main(flavor = "current_thread")]
    async fn exec(&self, args: &Respdiff) -> Result<()> {
        let env = args.env()?;
        let nsdb = database::open_db(&env, nssetsdb::NAME, true)?;

        let domain = respdiff::lowercase_dname(&self.domain)?;
        let resolver = Resolver::stub(self.resolver, self.port);
        let nsset = resolver.auth_nsset(&domain).await?;

        let entry = nssetsdb::NsSetEntry { domain, nsset };
        let mut txn = env.begin_rw_txn()?;
        nssetsdb::write(nsdb, &mut txn, &entry)?;
        txn.commit()?;

        info!("written NS set for \"{}\": {}", entry.domain, entry.nsset);

        Ok(())
    }
}

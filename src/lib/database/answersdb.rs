#![allow(rustdoc::invalid_rust_codeblocks)]
//! ``answers`` LMDB and its related data & functions
//!
//! ## ``answers`` binary format
//!
//! ``answers`` database stores the binary responses from the queried servers.
//!
//! If there are multiple servers, their responses are stored within a single
//! ``<QID>`` key.  Multiple responses are stored within the value by simply
//! concatenating them in the binary format of ``response`` described below.  Please
//! note the order of responses is significant and must correspond with the server
//! definition in the ``meta`` database.
//!
//! - key ``<QID>``
//!   - 4B unsigned int, little endian
//!   - DNS response(s) from server(s)
//!   - value: one or more ``response`` (see below)
//!
//! ``response`` represents a single DNS response from a server and has the
//! following binary format:
//!
//! ```notrust
//!   0    1    2    3    4    5    6      ...
//! +----+----+----+----+----+----+----\\----+
//! |        time       | length  |   wire   |
//! +----+----+----+----+----+----+----\\----+
//! ```
//!
//! ``time``
//!   - 4B unsigned int, little endian
//!   - time to receive the answer in microseconds
//!   - ``4294967295`` (``FF FF FF FF``) means *timeout*
//!
//! ``length``
//!   - 2B unsigned int, little endian
//!   - byte-length of the DNS ``wire`` format message that may follow
//!   - ``length`` is always present, even in case of *timeout*
//!
//! ``wire``
//!   - binary blob of ``length`` bytes
//!   - DNS wire format of the message received from server
//!   - ``wire`` is present only if ``length`` isn't zero

use crate::{
    DnsReply, ServerResponse, ServerResponseList,
    {error::DbFormatError, Error},
};
use byteorder::{ByteOrder, LittleEndian};
use domain::base::Message;
use lmdb::{Cursor, Database, RoTransaction, Transaction};
use std::time::Duration;

/// Answers LMDB database name
pub const NAME: &str = "answers";

/// Try to parse servers responses directly from LMDB binary data.
impl TryFrom<(&[u8], &[u8])> for ServerResponseList {
    type Error = DbFormatError;

    fn try_from(item: (&[u8], &[u8])) -> Result<Self, Self::Error> {
        let mut responses: Vec<ServerResponse> = vec![];
        let (key, buf) = item;
        if key.len() != 4 {
            return Err(DbFormatError::ReplyInvalidData);
        }

        let mut i = 0;
        while (i + 6) <= buf.len() {
            let delay = LittleEndian::read_u32(&buf[i..i + 4]);
            i += 4;
            let len = LittleEndian::read_u16(&buf[i..i + 2]) as usize;
            i += 2;

            if delay == u32::MAX {
                if len != 0 {
                    return Err(DbFormatError::ReplyInvalidData);
                } else {
                    responses.push(ServerResponse::Timeout);
                    continue;
                }
            }

            if i + len > buf.len() {
                return Err(DbFormatError::ReplyMissingData);
            }

            let wire: Vec<u8> = Vec::from(&buf[i..i + len]);
            i += len;

            match Message::from_octets(wire) {
                Ok(msg) => {
                    responses.push(ServerResponse::Data(DnsReply {
                        delay: Duration::from_micros(delay as u64),
                        message: msg,
                    }));
                }
                Err(_) => {
                    responses.push(ServerResponse::Malformed);
                }
            }
        }

        if i == buf.len() {
            Ok(ServerResponseList {
                key: LittleEndian::read_u32(key),
                responses,
            })
        } else {
            Err(DbFormatError::ReplyMissingData)
        }
    }
}

/// Retrieve server responses for all queries.
pub fn get_response_lists(
    db: Database,
    txn: &RoTransaction,
) -> Result<Vec<ServerResponseList>, Error> {
    let mut cur = txn.open_ro_cursor(db)?;
    let mut lists: Vec<_> = Vec::new();

    for res in cur.iter() {
        lists.push(ServerResponseList::try_from(res?)?);
    }
    Ok(lists)
}

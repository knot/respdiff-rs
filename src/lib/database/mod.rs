use lmdb::{Database, DatabaseFlags, Environment, Error as LmdbError};
use std::path::Path;

use crate::Error;

pub mod answersdb;
pub mod metadb;
pub mod nssetsdb;
pub mod queriesdb;
pub mod questionsdb;

/// Version string of supported respdiff db.
pub const BIN_FORMAT_VERSION: &str = "2018-05-21";

/// Create an LMDB Environment.
///
/// Only a single instance can exist in a process.
pub fn open_env(dir: &Path) -> Result<Environment, Error> {
    Ok(Environment::new()
        .set_max_dbs(5)
        .set_map_size(10 * 1024_usize.pow(3)) // 10 G
        .set_max_readers(384)
        .open(dir)?)
}

/// Create or open an LMDB database.
pub fn open_db(env: &Environment, name: &str, create: bool) -> Result<Database, Error> {
    if create {
        Ok(env.create_db(Some(name), DatabaseFlags::empty())?)
    } else {
        Ok(env.open_db(Some(name))?)
    }
}

/// Check if database exists already.
pub fn exists_db(env: &Environment, name: &str) -> Result<bool, Error> {
    match env.open_db(Some(name)) {
        Ok(_) => Ok(true),
        Err(LmdbError::NotFound) => Ok(false),
        Err(e) => Err(Error::Database(e)),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::error::DbFormatError;
    use lmdb::{Error as LmdbError, Transaction};
    use tempdir::TempDir;

    #[test]
    fn metadb_version() {
        let dir = TempDir::new("test").unwrap();
        let env = open_env(dir.path()).unwrap();
        let db = open_db(&env, metadb::NAME, true).unwrap();

        let mut txn = env.begin_rw_txn().unwrap();
        metadb::write_version(db, &mut txn).unwrap();
        txn.commit().unwrap();

        let txn = env.begin_ro_txn().unwrap();
        let version = metadb::check_version(db, &txn).unwrap();
        assert_eq!(version, BIN_FORMAT_VERSION);
    }

    #[test]
    fn exists() {
        let dir = TempDir::new("test").unwrap();
        let env = open_env(dir.path()).unwrap();
        let _d1 = open_db(&env, "d1", true).unwrap();

        assert_eq!(exists_db(&env, "d1").unwrap(), true);
        assert_eq!(exists_db(&env, "x").unwrap(), false);

        // trigger DbsFull becuase we set db limit to 5
        let _d2 = open_db(&env, "d2", true).unwrap();
        let _d3 = open_db(&env, "d3", true).unwrap();
        let _d4 = open_db(&env, "d4", true).unwrap();
        let _d5 = open_db(&env, "d5", true).unwrap();

        assert_eq!(
            exists_db(&env, "x"),
            Err(Error::Database(LmdbError::DbsFull))
        );
    }

    #[test]
    fn parse_serverreplylist() {
        use crate::{DnsReply, ServerResponse, ServerResponseList};
        use domain::base::Message;
        use std::time::Duration;

        let key = vec![0x42, 0x00, 0x00, 0x00];
        let empty = vec![];
        assert_eq!(
            ServerResponseList::try_from((key.as_slice(), empty.as_slice())),
            Ok(ServerResponseList {
                key: 0x42,
                responses: vec![]
            })
        );

        let timeout = vec![0xff, 0xff, 0xff, 0xff, 0x00, 0x00];
        assert_eq!(
            ServerResponseList::try_from((key.as_slice(), timeout.as_slice())),
            Ok(ServerResponseList {
                key: 0x42,
                responses: vec![ServerResponse::Timeout,],
            })
        );

        let missingdata = vec![0x00, 0x00, 0x00, 0x00, 0x01, 0x00];
        assert_eq!(
            ServerResponseList::try_from((key.as_slice(), missingdata.as_slice())),
            Err(DbFormatError::ReplyMissingData.into())
        );

        let shortdata = vec![0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00];
        assert_eq!(
            ServerResponseList::try_from((key.as_slice(), shortdata.as_slice())),
            Ok(ServerResponseList {
                key: 0x42,
                responses: vec![ServerResponse::Malformed],
            })
        );

        let wire = vec![
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c,
        ];
        let header = vec![0x00, 0x00, 0x00, 0x00, 0x0c, 0x00];
        let mut data = header.to_owned();
        data.append(&mut wire.to_owned());
        let dnsreply = DnsReply {
            delay: Duration::from_micros(0),
            message: Message::from_octets(wire.to_owned()).unwrap(),
        };
        assert_eq!(
            ServerResponseList::try_from((key.as_slice(), data.as_slice())),
            Ok(ServerResponseList {
                key: 0x42,
                responses: vec![ServerResponse::Data(dnsreply.to_owned())],
            })
        );

        data.append(&mut timeout.to_owned());
        let header3 = vec![0x01, 0x00, 0x00, 0x00, 0x0c, 0x00];
        data.append(&mut header3.to_owned());
        data.append(&mut wire.to_owned());
        assert_eq!(
            ServerResponseList::try_from((key.as_slice(), data.as_slice())),
            Ok(ServerResponseList {
                key: 0x42,
                responses: vec![
                    ServerResponse::Data(dnsreply.to_owned()),
                    ServerResponse::Timeout,
                    ServerResponse::Data(DnsReply {
                        delay: Duration::from_micros(1),
                        message: Message::from_octets(wire.to_owned()).unwrap(),
                    }),
                ],
            })
        );
    }
}

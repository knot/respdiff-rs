//! ``questions`` LMDB and its related data & functions
//!
//! ## ``questions`` binary format
//!
//! ``questions`` database stores a hash map of DNS queries indexed by ``<qname,
//! qtype, qclass>`` (aka ``question``). Every unique question may have a single
//! DNS query associated with it.
//!
//! Key ``<qname, qtype, qclass>`` conforms to RFC1035 Section 4.1.2. The ``qname``
//! is fully expanded (no compression there).
//!
//! - key ``<qname, qtype, qclass>``
//!   - binary, RFC1035 4.1.2
//!   - Number of occurrences
//!   - value: 4B unsigned int, little endian

use crate::error::Error;
use byteorder::{ByteOrder, LittleEndian};
use lmdb::{Database, Error as LmdbError, RwTransaction, Transaction, WriteFlags};

/// Questions LMDB database name
pub const NAME: &str = "questions";

/// Question entry stored in LMDB.
///
/// Each question is identified by a (qname, qtype, qclass) in RFC 1035 sec 4.1.2 format, which
/// is the key, and the entry contains the number of occurrences.
#[derive(Debug, Clone)]
pub struct QuestionEntry {
    /// A single question as defined by RFC1035 section 4.1.2.
    pub qbuf: Vec<u8>,
    /// Number of occurrences in data set (weight).
    pub count: u32,
}
impl From<(&[u8], &[u8])> for QuestionEntry {
    fn from(item: (&[u8], &[u8])) -> Self {
        let (key, val) = item;
        QuestionEntry {
            qbuf: key.to_vec(),
            count: LittleEndian::read_u32(val),
        }
    }
}

/// Get number of question occurrences.
pub fn get_count(db: Database, txn: &impl Transaction, qbuf: &[u8]) -> Result<u32, Error> {
    match txn.get(db, &qbuf) {
        Ok(buf) => Ok(LittleEndian::read_u32(buf)),
        Err(LmdbError::NotFound) => Ok(0),
        Err(e) => Err(Error::Database(e)),
    }
}

/// Write a question to LMDB.
pub fn write(db: Database, txn: &mut RwTransaction, qentry: &QuestionEntry) -> Result<(), Error> {
    let mut buf = [0; 4];
    LittleEndian::write_u32(&mut buf, qentry.count);
    txn.put(db, &qentry.qbuf, &buf, WriteFlags::empty())?;
    Ok(())
}

/// Increment question count in LMDB, write if not present yet.
pub fn increment(db: Database, txn: &mut RwTransaction, qbuf: &[u8]) -> Result<u32, Error> {
    let count = get_count(db, txn, qbuf)? + 1;
    let mut buf = [0; 4];
    LittleEndian::write_u32(&mut buf, count);
    txn.put(db, &qbuf, &buf, WriteFlags::empty())?;
    Ok(count)
}

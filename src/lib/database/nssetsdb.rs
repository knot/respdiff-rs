//! ``nssets`` LMDB and its related data & functions
//!
//! ## ``nssets`` binary format
//!
//! ``nssets`` database contains a set of name server domain names that are
//! authoritative for the domain.
//!
//! Each nsset value is indexed by a domain name in DNS format (``qname``).
//!
//! The value may contain 0 to N valid canonically ordered domain names. Each
//! domain name must be valid and ends with an empty label. Another domain name
//! follows immediately if present.
//!
//! - key ``<qname>``
//!   - DNS label format
//!   - Zero or more NS domain name(s)
//!   - value: DNS label format(s)

use crate::error::Error;
use crate::NameServerSet;
use domain::base::name::Dname;
use lmdb::{Database, Error as LmdbError, RwTransaction, Transaction, WriteFlags};

/// Name server sets LMDB database name
pub const NAME: &str = "nssets";

/// Name server set entry for a domain as stored in LMDB.
///
/// Key is a domain name in DNS format.
/// Value is a set of canonically ordered domain names in binary label format.
#[derive(Debug, Clone)]
pub struct NsSetEntry {
    /// Domain name in DNS message format.
    pub domain: Dname<Vec<u8>>,
    pub nsset: NameServerSet,
}

/// Get NS set for domain from LMDB.
pub fn get_nsset(
    db: Database,
    txn: &impl Transaction,
    domain: &Dname<Vec<u8>>,
) -> Result<Option<NameServerSet>, Error> {
    match txn.get(db, domain) {
        Ok(buf) => match buf.len() {
            0 => Ok(None),
            _ => Ok(Some(buf.try_into()?)),
        },
        Err(LmdbError::NotFound) => Err(Error::NotFoundInDb(domain.to_vec(), NAME.into())),
        Err(e) => Err(Error::Database(e)),
    }
}

/// Write an nsset to LMDB.
pub fn write(db: Database, txn: &mut RwTransaction, entry: &NsSetEntry) -> Result<(), Error> {
    let buf: Vec<u8> = (&entry.nsset).try_into()?;
    txn.put(db, &entry.domain, &buf, WriteFlags::empty())?;
    Ok(())
}

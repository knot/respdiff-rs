//! ``meta`` LMDB and its related data & functions
//!
//! ## ``meta`` binary format
//!
//! ``meta`` database stores additional information used for further processing of the data.
//!
//! - key ``version``
//!   - ASCII
//!   - respdiff binary format version
//!   - current version is [`database::BIN_FORMAT_VERSION`]
//!   - value: ASCII
//!
//! - key ``servers``
//!   - ASCII
//!   - number of servers responses are collected from
//!   - value: 4B unsigned int, little endian
//!
//! - key ``name0``, ``name1``, ..., ``name<N>``
//!   - ASCII
//!   - respdiff binary format version
//!   - name identifier of the n-th server (same as in ``respdiff.cfg``)
//!   - value: ASCII
//!
//! - key ``start_time``
//!   - ASCII
//!   - *optional*
//!   - unix timestamp of the start of data collection
//!   - value: 4B unsigned int, little endian
//!
//! - key ``end_time``
//!   - ASCII
//!   - *optional*
//!   - unix timestamp of the end of data collection
//!   - value: 4B unsigned int, little endian

use crate::{database, error::DbFormatError, Error};
use byteorder::{ByteOrder, LittleEndian};
use lmdb::{Database, RoTransaction, RwTransaction, Transaction, WriteFlags};
use std::time::SystemTime;

/// Meta LMDB database name
pub const NAME: &str = "meta";

/// Write binary format version to LMDB.
pub fn write_version(db: Database, txn: &mut RwTransaction) -> Result<(), Error> {
    Ok(txn.put(
        db,
        b"version",
        &database::BIN_FORMAT_VERSION,
        WriteFlags::empty(),
    )?)
}

/// Write start time when transciever started sending queries to LMDB.
pub fn write_start_time(db: Database, txn: &mut RwTransaction) -> Result<(), Error> {
    let duration = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(val) => val,
        Err(_) => return Err(Error::Time),
    };
    let ts: u32 = match duration.as_secs().try_into() {
        Ok(val) => val,
        Err(_) => return Err(Error::Time),
    };
    let mut bytes = [0; 4];
    LittleEndian::write_u32(&mut bytes, ts);
    Ok(txn.put(db, b"start_time", &bytes, WriteFlags::empty())?)
}

/// Read the transciever's start time.
pub fn read_start_time(db: Database, txn: &RoTransaction) -> Result<u32, Error> {
    let time = txn.get(db, b"start_time")?;
    Ok(LittleEndian::read_u32(time))
}

/// Write end time when transciever finished receiving queries to LMDB.
pub fn write_end_time(db: Database, txn: &mut RwTransaction) -> Result<(), Error> {
    let duration = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(val) => val,
        Err(_) => return Err(Error::Time),
    };
    let ts: u32 = match duration.as_secs().try_into() {
        Ok(val) => val,
        Err(_) => return Err(Error::Time),
    };
    let mut bytes = [0; 4];
    LittleEndian::write_u32(&mut bytes, ts);
    Ok(txn.put(db, b"end_time", &bytes, WriteFlags::empty())?)
}

/// Read the transceiver's end time.
pub fn read_end_time(db: Database, txn: &RoTransaction) -> Result<u32, Error> {
    let time = txn.get(db, b"end_time")?;
    Ok(LittleEndian::read_u32(time))
}

/// Check binary format version.
///
/// Perform a check that the binary version of particular LMDB is compatible
/// with the expected version.
pub fn check_version(db: Database, txn: &RoTransaction) -> Result<String, Error> {
    let version = txn.get(db, b"version")?;
    let version = String::from_utf8(version.to_vec())?;

    if version == database::BIN_FORMAT_VERSION {
        Ok(version)
    } else {
        Err(DbFormatError::Unsupported.into())
    }
}

/// Write a server list to LMDB.
pub fn write_servers(
    db: Database,
    txn: &mut RwTransaction,
    servers: Vec<String>,
) -> Result<(), Error> {
    let mut bytes = [0; 4];
    LittleEndian::write_u32(&mut bytes, servers.len() as u32);
    txn.put(db, b"servers", &bytes, WriteFlags::empty())?;

    for (i, name) in servers.into_iter().enumerate() {
        txn.put(db, &format!("name{}", i), &name, WriteFlags::empty())?;
    }
    Ok(())
}

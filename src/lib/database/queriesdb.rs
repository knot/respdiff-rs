//! ``queries`` LMDB and its related data & functions
//!
//! ## ``queries`` binary format
//!
//! ``queries`` database is used to store the wire format of queries that were sent
//! to the servers. Each query has a unique integer identifier, ``<QID>``.
//!
//! - key: ``<QID>``
//!   - 4B unsigned int, little endian
//!   - value: DNS query sent to server(s); DNS wire format

use crate::Error;
use crate::QKey;
use byteorder::{ByteOrder, LittleEndian};
use lmdb::{Cursor, Database, RoTransaction, Transaction};

/// Queries LMDB database name
pub const NAME: &str = "queries";

/// Query stored in LMDB.
///
/// Each query is identified by `QKey`, which is the key under which it is stored in the
/// ``queries`` database.
#[derive(Debug, Clone)]
pub struct Query {
    /// Identifier which is used in the ``queries`` LMDB.
    pub key: QKey,
    /// Binary data of the DNS message.
    pub wire: Vec<u8>,
}

impl From<(&[u8], &[u8])> for Query {
    fn from(item: (&[u8], &[u8])) -> Self {
        let (key, val) = item;
        Query {
            key: LittleEndian::read_u32(key),
            wire: val.to_vec(),
        }
    }
}

/// Retrieve all queries.
pub fn get_queries(db: Database, txn: &RoTransaction) -> Result<Vec<Query>, Error> {
    let mut cur = txn.open_ro_cursor(db)?;
    let mut queries: Vec<_> = Vec::new();

    for res in cur.iter() {
        queries.push(Query::from(res?));
    }
    Ok(queries)
}

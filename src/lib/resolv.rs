use domain::base::{
    iana::class::Class, iana::rtype::Rtype, message::RecordSection, octets::OctetsRef,
    question::Question as DomainQuestion, Dname, Message, ToDname,
};
use domain::rdata::rfc1035::Ns;
use domain::resolv::stub::{
    conf::{ResolvConf, ServerConf, Transport},
    StubResolver,
};
use std::net::{IpAddr, SocketAddr};

use crate::{error::ParseError, Error, NameServerSet};

/// A resolver with respdiff-specific utility functions.
#[derive(Debug, Clone)]
pub struct Resolver {
    stub: StubResolver,
    // TODO add customizable timeout?
}

impl Resolver {
    /// Instantiate a new stub resolver.
    pub fn stub(ip: Option<IpAddr>, port: u16) -> Self {
        let stub = {
            if let Some(ipaddr) = ip {
                let mut resolvconf = ResolvConf::new();
                let addr = SocketAddr::new(ipaddr, port);
                resolvconf
                    .servers
                    .push(ServerConf::new(addr, Transport::Udp));
                resolvconf.finalize();
                StubResolver::from_conf(resolvconf)
            } else {
                StubResolver::new()
            }
        };
        Resolver { stub }
    }

    /// Obtain authoritative name server set for given domain.
    pub async fn auth_nsset(&self, domain: &Dname<Vec<u8>>) -> Result<NameServerSet, Error> {
        let question = Question::lowercase(domain, Rtype::Ns, Class::In)?;
        let response = self.stub.query(question).await;
        let message = response.map_err(Error::NetworkError)?.as_ref().to_owned();
        let answer = message.answer()?;

        let nsset = extract_nsset(answer)?;

        if nsset.is_empty() {
            // no NS record -> query NS for SOA owner
            let authority = answer.next_section()?.ok_or(ParseError::AuthoritySection)?;
            for record in authority {
                let parsed = record?;
                if parsed.rtype() == Rtype::Soa {
                    let soa_owner: Dname<Vec<u8>> = parsed.owner().to_dname()?;
                    let question = Question::lowercase(&soa_owner, Rtype::Ns, Class::In)?;
                    let response = self.stub.query(question).await;
                    let message = response.map_err(Error::NetworkError)?.as_ref().to_owned();
                    let answer = message.answer()?;

                    return extract_nsset(answer);
                }
            }
        }
        Ok(nsset)
    }
}

/// DNS question as defined by RFC1035 section 4.1.2.
#[derive(Debug, Clone)]
pub struct Question {
    /// Query name
    pub qname: Dname<Vec<u8>>,
    /// Query type
    pub qtype: Rtype,
    /// Query class
    pub qclass: Class,
}
impl Question {
    /// Create a new question and normalize (lowercase) the query.
    pub fn lowercase<Ref: AsRef<[u8]>>(
        qname: &Dname<Ref>,
        qtype: Rtype,
        qclass: Class,
    ) -> Result<Self, Error> {
        Ok(Question {
            qname: crate::lowercase_dname(qname)?,
            qtype,
            qclass,
        })
    }
    /// Get a copy of the question buffer containing qname, qtype & qclass.
    pub fn qbuf(&self) -> Vec<u8> {
        use byteorder::{ByteOrder, NetworkEndian};
        let mut qvec = self.qname.as_octets().clone();
        qvec.extend(vec![0, 0, 0, 0]);
        let len = qvec.len();
        NetworkEndian::write_u16(&mut qvec[len - 4..len - 2], self.qtype.to_int());
        NetworkEndian::write_u16(&mut qvec[len - 2..], self.qclass.to_int());
        qvec
    }
}
impl TryFrom<Message<Vec<u8>>> for Question {
    type Error = Error;

    fn try_from(msg: Message<Vec<u8>>) -> Result<Self, Self::Error> {
        let question = msg.question().next().ok_or(ParseError::QuestionSection)??;
        let qname: Dname<Vec<u8>> = question.qname().to_dname()?;
        Question::lowercase(&qname, question.qtype(), question.qclass())
    }
}
impl From<Question> for DomainQuestion<Dname<Vec<u8>>> {
    fn from(question: Question) -> Self {
        DomainQuestion::new(question.qname, question.qtype, question.qclass)
    }
}

fn extract_nsset<T>(section: RecordSection<T>) -> Result<NameServerSet, Error>
where
    T: OctetsRef,
{
    let mut nsset = NameServerSet::new();
    for record in section {
        let parsed = record?;
        if parsed.rtype() == Rtype::Ns {
            if let Some(nsrec) = parsed.into_record::<Ns<_>>()? {
                let nsdname = nsrec.data().nsdname();
                nsset.insert(nsdname.to_dname()?);
            }
        }
    }
    Ok(nsset)
}

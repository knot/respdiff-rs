use domain::base::name;
use domain::base::octets::{self, ShortBuf};
use serde_ini::de;
use std::io;
use std::string::FromUtf8Error;
use thiserror::Error;

/// Error for respdiff operation.
///
/// This is a catch-all error for the respdiff library. All possible errors that may occur during
/// respdiff operation should be convertable to this `Error` type.
#[derive(Error, Debug)]
pub enum Error {
    #[error("LMDB error: {0}")]
    Database(#[from] lmdb::Error),
    #[error("key not found in LMDB \"{1}\"")]
    NotFoundInDb(Vec<u8>, String),
    #[error("database format error: {0}")]
    DbFormatError(DbFormatError),
    #[error("failed to obtain current time")]
    Time,
    #[error("non-ascii characters in conversion: {0}")]
    NonAscii(#[from] FromUtf8Error),
    #[error("unknown transport protocol: {0}")]
    UnknownTransportProtocol(String),
    #[error("unknown diff criteria: {0}")]
    UnknownDiffCriteria(String),
    #[error("unknown field weight: {0}")]
    UnknownFieldWeight(String),
    #[error("failed to open config file: {0}")]
    ConfigFile(io::Error),
    #[error("failed to parse config file: {0}")]
    ConfigRead(de::Error),
    #[error("invaliad server name")]
    InvalidServerName,
    #[error("functionality not yet implemented")]
    NotImplemented,
    #[error("failed to write datafile: {0}")]
    DatafileWrite(io::Error),
    #[error("failed to serialize datafile into JSON: {0}")]
    DatafileSerialize(#[from] serde_json::Error),
    #[error("pcap error: {0}")]
    PcapError(PcapError),
    #[error("failed to parse DNS message: {0}")]
    ParseError(ParseError),
    #[error("network error: {0}")]
    NetworkError(io::Error),
}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        use Error::*;
        match (self, other) {
            (Database(a), Database(b)) => a == b,
            (DbFormatError(a), DbFormatError(b)) => a == b,
            (NotFoundInDb(k, db), NotFoundInDb(k2, db2)) => k == k2 && db == db2,
            (Time, Time) => true,
            (NonAscii(a), NonAscii(b)) => a == b,
            (UnknownTransportProtocol(a), UnknownTransportProtocol(b)) => a == b,
            (UnknownDiffCriteria(a), UnknownDiffCriteria(b)) => a == b,
            (UnknownFieldWeight(a), UnknownFieldWeight(b)) => a == b,
            (ConfigFile(_), ConfigFile(_)) => true,
            (ConfigRead(_), ConfigRead(_)) => true,
            (NotImplemented, NotImplemented) => true,
            (DatafileWrite(_), DatafileWrite(_)) => true,
            (DatafileSerialize(_), DatafileSerialize(_)) => true,
            _ => false,
        }
    }
}
impl Eq for Error {}
impl From<DbFormatError> for Error {
    fn from(error: DbFormatError) -> Self {
        Self::DbFormatError(error)
    }
}
impl From<PcapError> for Error {
    fn from(error: PcapError) -> Self {
        Self::PcapError(error)
    }
}
impl From<ParseError> for Error {
    fn from(error: ParseError) -> Self {
        Self::ParseError(error)
    }
}
impl From<ShortBuf> for Error {
    fn from(error: ShortBuf) -> Self {
        Self::ParseError(error.into())
    }
}
impl From<name::DnameError> for Error {
    fn from(error: name::DnameError) -> Self {
        Self::ParseError(error.into())
    }
}
impl From<octets::ParseError> for Error {
    fn from(error: octets::ParseError) -> Self {
        Self::ParseError(error.into())
    }
}
impl From<name::PushError> for Error {
    fn from(error: name::PushError) -> Self {
        Self::ParseError(error.into())
    }
}

#[derive(Error, Debug, PartialEq, Eq)]
pub enum DbFormatError {
    #[error("unsupported binary format version")]
    Unsupported,
    #[error("reply in answers db is missing data")]
    ReplyMissingData,
    #[error("reply in answers db contains invalid data")]
    ReplyInvalidData,
}

#[derive(Error, Debug, PartialEq, Eq)]
pub enum PcapError {
    #[error("unsupported linktype: {0}({1})")]
    UnsupportedLinktype(String, i32),
    #[error("packet too short")]
    ShortPacket,
    #[error("only IPv4 and IPv6 protocols supported")]
    UnsupportedNetwork,
    #[error("only UDP is supported")]
    UnsupportedTransport,
}

#[derive(Error, Debug, PartialEq, Eq)]
pub enum ParseError {
    #[error("invalid length")]
    InvalidLength,
    #[error("invalid question section")]
    QuestionSection,
    #[error("invalid authority section")]
    AuthoritySection,
    #[error("invalid domain name: {0}")]
    DomainName(name::DnameError),
    #[error("{0}")]
    SectionError(octets::ParseError),
    #[error("{0}")]
    PushError(name::PushError),
}
impl From<ShortBuf> for ParseError {
    fn from(_: ShortBuf) -> Self {
        Self::InvalidLength
    }
}
impl From<octets::ParseError> for ParseError {
    fn from(error: octets::ParseError) -> Self {
        Self::SectionError(error)
    }
}
impl From<name::DnameError> for ParseError {
    fn from(error: name::DnameError) -> Self {
        Self::DomainName(error)
    }
}
impl From<name::PushError> for ParseError {
    fn from(error: name::PushError) -> Self {
        Self::PushError(error)
    }
}

use crate::Error;
use domain::base::name::Dname;
use domain::base::octets::{Compose, IntoBuilder, OctetsBuilder, Parse, Parser};
use std::collections::BTreeSet;

/// Set of name servers.
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct NameServerSet {
    dnames: BTreeSet<Dname<Vec<u8>>>,
}
impl NameServerSet {
    pub fn new() -> Self {
        Self {
            dnames: BTreeSet::new(),
        }
    }

    pub fn insert(&mut self, server: Dname<Vec<u8>>) {
        self.dnames.insert(server);
    }

    pub fn remove(&mut self, server: &Dname<Vec<u8>>) {
        self.dnames.remove(server);
    }

    pub fn contains(&self, server: &Dname<Vec<u8>>) -> bool {
        self.dnames.contains(server)
    }

    pub fn len(&self) -> usize {
        self.dnames.len()
    }

    pub fn is_empty(&self) -> bool {
        self.dnames.is_empty()
    }
}
impl TryFrom<&NameServerSet> for Vec<u8> {
    type Error = Error;

    fn try_from(nsset: &NameServerSet) -> Result<Vec<u8>, Error> {
        let mut builder = vec![].into_builder();
        for dname in &nsset.dnames {
            dname.compose_canonical(&mut builder)?;
        }
        Ok(builder.freeze())
    }
}
impl TryFrom<&[u8]> for NameServerSet {
    type Error = Error;

    fn try_from(bytes: &[u8]) -> Result<NameServerSet, Error> {
        let mut nsset = NameServerSet::new();

        let mut cursor = 0;
        let mut parser = Parser::from_ref(bytes);
        while cursor < bytes.len() {
            let dname = Dname::parse(&mut parser)?;
            let len = dname.as_octets().len();
            let dname: Dname<Vec<u8>> = Dname::from_octets(bytes[cursor..cursor + len].into())?;
            nsset.insert(dname.to_owned());
            cursor += len;
        }

        Ok(nsset)
    }
}
impl std::fmt::Display for NameServerSet {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut iter = self.dnames.iter();
        if let Some(value) = iter.next() {
            write!(f, "{}", value)?;
        }
        for value in iter {
            write!(f, ", {}", value)?;
        }
        Ok(())
    }
}

#[allow(unused_imports)]
mod tests {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn nsset_into_octets() {
        // create new NameServerSet
        let mut name_server_set = NameServerSet::new();

        // check it converts into empty Vec<u8>
        let expected_empty_vec: Vec<u8> = vec![];
        let empty_vec: Vec<u8> = (&name_server_set).try_into().unwrap();
        assert_eq!(empty_vec, expected_empty_vec);

        // add nameservers
        let ns1: Dname<Vec<u8>> = Dname::from_str("ns1.test.").unwrap();
        let ns2: Dname<Vec<u8>> = Dname::from_str("ns2.test.").unwrap();
        let ns3: Dname<Vec<u8>> = Dname::from_str("ns3.test.").unwrap();
        name_server_set.insert(ns2.clone());
        name_server_set.insert(ns3.clone());
        name_server_set.insert(ns1.clone());

        let nsset: Vec<u8> = (&name_server_set).try_into().unwrap();
        assert_eq!(
            nsset.len(),
            ns1.as_octets().len() + ns2.as_octets().len() + ns3.as_octets().len()
        );

        // test conversions
        let nsset_repr: NameServerSet = (&nsset[..]).try_into().unwrap();
        assert_eq!(name_server_set, nsset_repr);
    }
}

# Respdiff (Rust)

This project is an extension of the
[respdiff](https://gitlab.nic.cz/knot/respdiff) toolchain. It works with the
same underlying LMDB and uses the same database format as the Python toolchain.
The tools that have been rewritten can be used interchangibly with the LMDB
created from the Python toolchain.

The aim of this Rust toolchain is to provide a faster, more reliable tools to
process the DNS and LMDB data respdiff works with. Compared to the original, it
should also provide a user-friendly CLI rather than assorted collection of
scripts.

## Development status

The tools should be considered a work in progress and subject to change unless
explicitly mentioned otherwise.

API for the respdiff library is very unstable and no promises are made wrt
compatibility between the 0.y versions.

## LMDB compatibility

`respdiff-rs` can work with the `respdiff` LMDB and vice versa. However,
`respdiff-rs` also adds new functionalities and databases that aren't
implemented in the Python toolchain.

## Tools

### diff-answers

The `diff-answers` tool is a rewrite of the original `msgdiff` tool from the
Python toolchain. It serves the same purpuse, but runs much quicker. It isn't as
feature-complete as the Python version, but it can serve as a drop-in
replacement for most common configurations. In our use, it proved to be two
orders of magnitude faster than the Python version.

The `diff-answers` is being used in testing and can be considered fairly
realiable.

### parse-questions

This tool processes an input PCAP to extract DNS questions from it. These are
stored in the LMDB for further processing and dataset preparation.

### snapshot-nsset

This tool can determine the authoritative name server set for a given domain by
sending queries to a given resolver. Results are stored in LMDB to be used for
further dataset processing.
